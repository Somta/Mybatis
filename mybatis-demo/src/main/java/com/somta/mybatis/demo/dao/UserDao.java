package com.somta.mybatis.demo.dao;

import com.somta.mybatis.demo.pojo.User;

/**
 * Created by Kevin on 2019/1/14.
 */
public interface UserDao {

    User getUser(Integer id);


}
